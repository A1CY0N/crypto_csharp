// ===============================
// AUTHOR          : A1CY0N
// CREATE DATE     : Thu 03 Oct 2019 09∶23∶44 PM CEST
// PURPOSE         : Implementation of a Diffie-Hellmann key exchange in C#
// ===============================
using System;

namespace ConsoleApp
{
    public class Program
    {
       public static void Main(string[] args)
        { 
            // P - Modular (prime number)
            int P = DiffieHellman.generateRandomPrimeNumber(10,100);
            Console.WriteLine(String.Format("The value of P is {0}.\n",P));

            // G
            int G = DiffieHellman.RandomNumber(1000,10000);
            Console.WriteLine(String.Format("The value of G is {0}.\n",G));
            
            int a, b;	// a - Alice's Secret Key, b - Bob's Secret Key.
	        int A, B;	// A - Alice's Public Key, B - Bob's Public Key


            // Random int for Alice's private Key
            a = DiffieHellman.RandomNumber(1000,100000);;
            Console.WriteLine(String.Format("The value of a is {0}.\n",a));

            // Alice's Public Key (Alice will send A to Bob)
            A = DiffieHellman.power(G, a, P);
            Console.WriteLine(String.Format("The value of A is {0}.\n",A));

            // Random int for Bob's Pivate Key
            b = DiffieHellman.RandomNumber(1000,100000);;
            Console.WriteLine(String.Format("The value of b is {0}.\n",b));

            // Calculate Bob's Public Key (Bob will send B to Alice)
            B = DiffieHellman.power(G, b, P);
            Console.WriteLine(String.Format("The value of B is {0}.\n",B));

            // Calculate Secret shared key
            int keyAlice = DiffieHellman.power(B, a, P);
            int keyBob = DiffieHellman.power(A, b, P);

            Console.WriteLine(String.Format("Alice's Secret Key is {0}\nBob's Secret Key is {1}.\n", keyAlice, keyBob));
        }

        
        public class DiffieHellman
        {
            /// <summary> 
            /// Perform Diffie Hellman key exchange
            /// </summary>
            public static int power(int a, int b, int p) 
            { 
                // Calculate a ^ b mod p
                int res = 1;      
                
                a = a % p;  
            
                while (b > 0) 
                { 
                    if((b & 1) == 1) 
                        res = (res * a) % p; 
            
                    b = b >> 1;  
                    a = (a * a) % p;  
                } 
                return res; 
            } 

            private static bool checkIfPrime(int n)
            {
                // Check if n is prime
                var sqrt = Math.Sqrt(n);
                if ((n % 2) == 0) return false;
                for (var i = 3; i <= sqrt; i+= 2)
                    if ((n % i) == 0) return false;
                return true;
            }

            public static int RandomNumber(int min, int max)  
            {  
                // Generate random number between min and max
                Random random = new Random();  
                return random.Next(min, max);  
            } 
            public static int generateRandomPrimeNumber(int min, int max)  
            {  
                // Generate random prime number
                int number;
                do{
                    number = RandomNumber(min, max);
                }while (!DiffieHellman.checkIfPrime(number));
                return number;
            } 
        } 
    }
}

