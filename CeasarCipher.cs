﻿// ===============================
// AUTHOR          : A1CY0N
// CREATE DATE     : Thu 03 Oct 2019 09∶56∶34 PM CEST
// PURPOSE         : Implementation of a Cesar encryption by monoalphabetic shifting in C#
// ===============================
using System;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string command;
            do
            {
                Console.WriteLine("Ceasar Cipher!");

                Console.WriteLine("E Encrypt");
                Console.WriteLine("D Decrypt");
                Console.WriteLine("----------");
                Console.WriteLine("X Exit");

                command = Console.ReadLine();

                switch (command)
                {
                    case "E":
                        RunCeasarEncrypt();
                        break;
                    case "D":
                        RunCeasarDecrypt();
                        break;
                }
            } while (command!= "X");
        }

        static void RunCeasarEncrypt()
        {
            Console.WriteLine("Text to encrypt");
            Console.WriteLine(">");
            string plainText = Console.ReadLine()?.Trim().ToUpper() ?? "";

            Console.WriteLine("Amount to shift");
            Console.WriteLine(">");
            string shiftAmountText = Console.ReadLine();

            var shiftAmount = 0;
            if (int.TryParse(shiftAmountText, out shiftAmount))
            {
                if(shiftAmount > 0)
                {
                    string encryptedtext = CeasarEncrypt(plainText, shiftAmount);
                    Console.WriteLine("-------------");
                    Console.WriteLine("shift:        " + shiftAmount);
                    Console.WriteLine("original:        " + plainText);
                    Console.WriteLine("encrypted:        " + encryptedtext);
                    Console.WriteLine("-------------");
                }
                else
                {
                    Console.WriteLine("Error : " + shiftAmountText + " is not a positif number");
                }
            }
            else
            {
                Console.WriteLine("Error : " + shiftAmountText + " is not a number");
            }
        }
        
        static void RunCeasarDecrypt()
        {
            Console.WriteLine("Text to decrypt");
            Console.WriteLine(">");
            string encryptedtext = Console.ReadLine()?.Trim().ToUpper() ?? "";

            Console.WriteLine("Amount to shift");
            Console.WriteLine(">");
            string shiftAmountText = Console.ReadLine();

            var shiftAmount = 0;
            if (int.TryParse(shiftAmountText, out shiftAmount))
            {
                if(shiftAmount > 0)
                {
                    string plainText = CeasarDecrypt(encryptedtext, shiftAmount);
                    Console.WriteLine("-------------");
                    Console.WriteLine("shift:        " + shiftAmount);
                    Console.WriteLine("encrypted:        " + encryptedtext);
                    Console.WriteLine("original:        " + plainText);
                    Console.WriteLine("-------------");
                }
                else
                {
                    Console.WriteLine("Error : " + shiftAmountText + " is not a positif number");
                }
            }
            else
            {
                Console.WriteLine("Error : " + shiftAmountText + " is not a number");
            }
        }
        

        private static readonly char[] _alphabet = new char[]
        {
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
        };
        
        static string CeasarEncrypt(string plainText, int shiftAmount)
        {
            string result = "";
            foreach (var letter in plainText)
            {
                var indexInAlphabet = Array.IndexOf(_alphabet, letter);
                indexInAlphabet = (indexInAlphabet + shiftAmount) % _alphabet.Length;
                result += _alphabet[indexInAlphabet];
            }

            return result;
        }
        
        static string CeasarDecrypt(string encrypted, int shiftAmount)
        {
            string result = "";
            foreach (var letter in encrypted)
            {
                
                var indexInAlphabet = Array.IndexOf(_alphabet, letter);
                indexInAlphabet = (indexInAlphabet - shiftAmount) % _alphabet.Length;
                /*
                if (indexInAlphabet < shiftAmount)
                {
                    indexInAlphabet =(_alphabet.Length - shiftAmount) % _alphabet.Length;
                }
                else
                {
                    indexInAlphabet = (indexInAlphabet - shiftAmount) % _alphabet.Length;
                }*/

                while (indexInAlphabet < 0)
                {
                    indexInAlphabet += _alphabet.Length;
                }
                result += _alphabet[indexInAlphabet];
            }

            return result;
        }
    }
}

