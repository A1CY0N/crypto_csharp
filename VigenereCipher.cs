﻿// ===============================
// AUTHOR          : A1CY0N
// CREATE DATE     : Thu 03 Oct 2019 11∶24∶57 PM CEST
// PURPOSE         : Implementation of Vigenere polyalphabetic substitution ciphering in C#
// ===============================
using System;
using System.Text.RegularExpressions;

namespace ConsoleApp
{
    public class Program
    {
       public static void Main(string[] args)
        {
            string command;
            do
            {
                Console.WriteLine("Hello World!");

                Console.WriteLine("E Encrypt");
                Console.WriteLine("D Decrypt");
                Console.WriteLine("----------");
                Console.WriteLine("X Exit");

                command = Console.ReadLine();
               
                switch (command)
                {
                    case "E":
                        VigenereCipher.RunVigenereEncrypt();
                        break;
                    case "D":
                        VigenereCipher.RunVigenereDecrypt();
                        break;
                }
            } while (command!= "X");
        }

        
        public class VigenereCipher
        {
            /// <summary> 
            /// Perform Vigenere ciphering
            /// </summary>
            private static string PrepareInput(string input)
            {
                // Sanitize the input
                var regex = new Regex("[^A-Z]");
                return regex.Replace(input.ToUpper(), string.Empty);
            }
            public static void RunVigenereEncrypt()
            {
                // Perform plaintext input and encryption
                Console.WriteLine("Text to encrypt");
                Console.WriteLine(">");
                string plainText = Console.ReadLine()?.Trim().ToUpper() ?? "";

                while (string.IsNullOrEmpty(plainText))
                {
                    Console.WriteLine("Text can't be empty! Input your text once more");
                    plainText = Console.ReadLine()?.Trim().ToUpper() ?? "";
                }
                plainText = PrepareInput(plainText);

                Console.WriteLine("Key");
                Console.WriteLine(">");
                string key = Console.ReadLine()?.Trim().ToUpper() ?? "";

                while (string.IsNullOrEmpty(key))
                {
                    Console.WriteLine("Key can't be empty! Input your key once more");
                    key = Console.ReadLine()?.Trim().ToUpper() ?? "";
                }
                key = PrepareInput(key);

                if (plainText.Length == key.Length)
                {
                    string encryptedtext = VigenereEncrypt(plainText, key);
                    Console.WriteLine("-------------");
                    Console.WriteLine("Key:        " + key);
                    Console.WriteLine("original:        " + plainText);
                    Console.WriteLine("encrypted:        " + encryptedtext);
                    Console.WriteLine("-------------");
                } 
                else
                {
                    throw new System.ArgumentException("Error : " + plainText + " and " + key + " don't have the same length");
                }
            }
            
            public static void RunVigenereDecrypt()
            {
                // Perform encrypted input and decryption
                Console.WriteLine("Text to decrypt");
                Console.WriteLine(">");
                string encryptedtext = Console.ReadLine()?.Trim().ToUpper() ?? "";

                while (string.IsNullOrEmpty(encryptedtext))
                {
                    Console.WriteLine("Text can't be empty! Input your text once more");
                    encryptedtext = Console.ReadLine()?.Trim().ToUpper() ?? "";
                }
                encryptedtext = PrepareInput(encryptedtext);

                Console.WriteLine("Key");
                Console.WriteLine(">");
                string key = Console.ReadLine()?.Trim().ToUpper() ?? "";

                while (string.IsNullOrEmpty(key))
                {
                    Console.WriteLine("Key can't be empty! Input your key once more");
                    key = Console.ReadLine()?.Trim().ToUpper() ?? "";
                }
                key = PrepareInput(key);

                if (encryptedtext.Length == key.Length)
                {
                    string plainText = VigenereDecrypt(encryptedtext, key);
                    Console.WriteLine("-------------");
                    Console.WriteLine("Key:        " + key);
                    Console.WriteLine("encrypted:        " + encryptedtext);
                    Console.WriteLine("original:        " + plainText);
                    Console.WriteLine("-------------");
                }
                else
                {
                    throw new System.ArgumentException("Error : " + encryptedtext + " and " + key + " don't have the same length");
                }
            }

            private static String VigenereEncrypt(String plainText, String key) 
            { 
                // Encryption
                String cipherText=""; 
            
                for (int i = 0; i < plainText.Length; i++) 
                { 
                    int tmpChar = (plainText[i] + key[i]) %26; 
            
                    tmpChar += 'A'; 
            
                    cipherText+=(char)(tmpChar);
                } 
                return cipherText; 
            }

            private static String VigenereDecrypt(String cipherText, String key) 
            { 
                // Decryption
                String plainText=""; 
            
                for (int i = 0 ; i < cipherText.Length &&  i < key.Length; i++) 
                { 
                    int tmpChar = (cipherText[i] -  
                                key[i] + 26) %26; 
                
                    tmpChar += 'A'; 
                    plainText+=(char)(tmpChar); 
                } 
                return plainText; 
            } 
        } 
    }
}

